package com.san.miniproject;

public class RestaurantsMenuCard {
	private int itemID, itemQuantity;
	private  double itemPrice;
		private  String itemname; 
		
		public RestaurantsMenuCard(int itemID, String itemname, int itemQuantity, double itemPrice) {
			super();
			this.itemID = itemID;
			this.itemname = itemname;
			this.itemQuantity = itemQuantity;
			this.itemPrice = itemPrice;
		}


public int getItemID() {
			return itemID;
		}


		public void setItemID(int itemID) {
			this.itemID = itemID;
		}


		public int getItemQuantity() {
			return itemQuantity;
		}


		public void setItemQuantity(int itemQuantity) {
			this.itemQuantity = itemQuantity;
		}


		public double getItemPrice() {
			return itemPrice;
		}


		public void setItemPrice(double itemPrice) {
			this.itemPrice = itemPrice;
		}


		public String getItemname() {
			return itemname;
		}


		public void setItemname(String itemname) {
			this.itemname = itemname;
		}
		
		@Override
		public String toString() {
			return "MenuCard [itemID=" + itemID + ", itemQuantity=" + itemQuantity + ", itemPrice=" + itemPrice
					+ ", itemname=" + itemname + "]";
		}

		public boolean validate(RestaurantsMenuCard item) {
			if(item.getItemID() >0 && item.getItemname()!=null && !( item.getItemname().isEmpty()) && item.getItemQuantity()>0  && item.getItemPrice() != 0) {
				return true;
			}
			else {
				throw new IllegalArgumentException("exception occured,Invalid Input");
			}
		}


		public static void add(RestaurantsMenuCard i3) {
			// TODO Auto-generated method stub
			
		}




}
