package com.san.miniproject;

import java.util.*;
import java.time.LocalDateTime;
public class RestaurantsBill {
			private String name;
			private List<RestaurantsMenuCard> items;
			private double cost;
			private Date time;
			private Object MenuCard;
			
			public RestaurantsBill() {}
			
			public RestaurantsBill(String name, List<RestaurantsMenuCard> items, double cost, Date time) throws IllegalArgumentException {
				super();
				
				this.name = name;
				this.MenuCard = MenuCard;
				if(cost<0)
				{
					throw new IllegalArgumentException("exception occured, cost cannot less than zero");
				}
				this.cost = cost;
				this.time = time;
			}
			
			public String getName() {
				return name;
			}
			public void setName(String name) {
				this.name = name;
			}
			
			public List<RestaurantsMenuCard> getItems() {
				return items;
			}
			public void setItems(List<RestaurantsMenuCard> selectedItems) {
				this.items = selectedItems;
			}
			public double getCost() {
				return cost;
			}
			public void setCost(double cost) {
				this.cost = cost;
			}
			
			public Date getTime() {
				return time;
			}
			public void setTime(Date date) {
				this.time = date;
			}
			

	}



